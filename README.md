![img](https://i.imgur.com/xqc8bGL.png)

## Getting Started
Check out the docs at https://docs.threatnote.io/ to get started!

## Quick Start

This will launch the following:

- Flask app running threatnote.io listening on port 5000 (access http://local.docker:5000 to login)
- Redis server to manage Redis workers
- A Redis worker named enricher to manage the enrichment jobs

For a general overview of the product, check out https://threatnote.io

Based on https://github.com/brianwarehime/threatnote
