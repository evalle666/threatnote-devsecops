stages:
  - build
  - automated code review
  - sensitive information scan
  - source composition analysis
  - static application security testing
  - test
  - push
  - deploy
  - dynamic app sec test
  - compliance as code


Openscap:
  image: ubuntu:latest
  stage: compliance as code
  before_script:
    - apt-get update -qq
    - apt-get install -qq git
    - export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true
  # Setup SSH deploy keys
    - 'which ssh-agent || ( apt-get install -qq openssh-client )'
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$SSH_PRIVATE_KEY" | base64 --decode)
    - mkdir -p ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - chmod 700 ~/.ssh
  # Install ansible
    - apt-get install software-properties-common -y
    - apt-add-repository --yes --update ppa:ansible/ansible
    - apt-get install ansible -y
    - echo -e "[server]\r\n$SERVER_IP\r\n\r\n[server:vars]\r\nansible_python_interpreter=/usr/bin/python3" > host.txt
  script:
    - ansible-playbook ./openscap.yml -i host.txt || true
  artifacts:
    paths: [openscap_analyze_result.html]
  only:
    - master


OwaspZap:
  stage: dynamic app sec test
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker pull ictu/zap2docker-weekly
  script:
    - docker run --user $(id -u):$(id -g) -v $(pwd):/zap/wrk/ -t ictu/zap2docker-weekly zap-baseline.py -I -j -t http://$PROD_URL/ -r owaspzap_analyze_results.html --hook=/zap/auth_hook.py -z "auth.loginurl=http://$PROD_URL/login auth.username="$PROD_TEST_USERNAME" auth.password="$PROD_TEST_PASS" auth.username_field="email" auth.password_field="password" auth.submit_field="kt_login_submit" auth.display=False" || true
  artifacts:
    paths: [owaspzap_analyze_results.html]
  only:
    - master


Bandit:
  stage: static application security testing
  image: python:latest
  before_script:
    - pip install bandit
  script:
    - bandit -r threatnote/ | tee bandit_analyze_results.txt || true
  artifacts:
    paths: [bandit_analyze_results.txt]
  only:
    - master


Safety:
  stage: source composition analysis
  image: python:latest
  before_script:
    - pip install safety
  script:
    - cd threatnote/
    - safety check -r requirements.txt --json | tee safety_analyze_results.json
  artifacts:
    paths: [safety_analyze_results.json] 
  only:
   - master


Trufflehog:
  stage: sensitive information scan
  image:
    name: registry.gitlab.com/evalle666/trufflehog:main
  script:
      - git checkout ${CI_COMMIT_REF_NAME}
      - trufflehog --regex --entropy=False ./ --json > trufflehog_analyze_results.json || true
  artifacts:
    paths: [trufflehog_analyze_results.json]
  only:
    - master


Devskim:
  stage: automated code review
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker pull evalle666/devskim
  script:
      - docker run -v $(pwd)/threatnote:/code evalle666/devskim analyze /code | tee devskim_analyze_results.txt
  artifacts:
    paths: [devskim_analyze_results.txt]
  only:
    - master


Build:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - cd threatnote/
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    # cache-from here tells a tagged image to be used as a cache source
    - |
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        tag=""
        echo "Running on default branch '$CI_DEFAULT_BRANCH': tag = 'latest'"
      else
        tag=":$CI_COMMIT_REF_SLUG"
        echo "Running on branch '$CI_COMMIT_BRANCH': tag = $tag"
      fi
    - docker build -t $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  only:
    - master


Selenium test:
  stage: test
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - apk add --update --no-cache py3-pip py3-setuptools python3 python3-dev chromium-chromedriver
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker run -d -p $APP_PORT:5000 --env SECRET_KEY=$SECRET_KEY --env SQLALCHEMY_DATABASE_URI=$SQLALCHEMY_TEST_DATABASE_URI $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - pip install pytest==6.2.5 selenium==3.141.0 webdriver-manager==3.4.2
  script:
    - cd tests/
    - pytest -s test_login.py
  only:
    - master


# Push latest Docker image to the registry
Push latest:
  stage: push
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:latest
    - docker push $CI_REGISTRY_IMAGE:latest
  only:
    - master


Deploy:
  image: ubuntu:latest
  stage: deploy
  before_script:
    - apt-get update -qq
    - apt-get install -qq git
    - export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true
  # Setup SSH deploy keys
    - 'which ssh-agent || ( apt-get install -qq openssh-client )'
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$SSH_PRIVATE_KEY" | base64 --decode)
    - mkdir -p ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - chmod 700 ~/.ssh
  # Install ansible
    - apt-get install software-properties-common -y
    - apt-add-repository --yes --update ppa:ansible/ansible
    - apt-get install ansible -y
    - echo -e "[server]\r\n$SERVER_IP\r\n\r\n[server:vars]\r\nansible_python_interpreter=/usr/bin/python3" > host.txt
  script:
    - DOCKER_IMAGE=$CI_REGISTRY_IMAGE:latest ansible-playbook ./playbook.yml -i host.txt
  only:
    - master
